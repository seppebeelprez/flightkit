@extends('layouts.guest')

@section('content')
<div class="">
    <div class="">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 col-xxs">
            <div class="login_block">
                <h2>Login</h2>

                <div class="login_form">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-xs-12">Enter your email*</label>

                            <div class="input-group col-xs-12">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope fa-2x"></i></span>
                                <input type="email" class="form-control" name="email" aria-describedby="basic-addon1" value="{{ old('email') }}">
                            </div>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-xs-12">Enter your password*</label>

                            <div class="input-group col-xs-12">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-2x"></i></span>
                                <input type="password" class="form-control" name="password" aria-describedby="basic-addon1">
                            </div>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-login">Login</button>
                            </div>
                        </div>

                        <div class="or_divider">
                            <p>or</p>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="redirect" class="btn btn-social">
                                    Login with facebook
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <div class="login_extra">
                                        <div class="col-xs-6">
                                            <label>
                                                <input type="checkbox" name="remember"> Remember Me
                                            </label>
                                        </div>

                                        <div class="col-xs-6">
                                            <a class="btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                {{--<div class="login_form">--}}
                    {{--<form>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-xs-12">--}}
                                {{--<a href="redirect">--}}
                                    {{--<button class="btn btn-social">Login with facebook</button>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-xs-12">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<div class="login_extra">--}}
                                        {{--<div class="col-xs-6">--}}
                                            {{--<label>--}}
                                                {{--<input type="checkbox" name="remember"> Remember Me--}}
                                            {{--</label>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-xs-6">--}}
                                            {{--<a class="btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            </div>
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Login</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">--}}
                        {{--{{ csrf_token() }}--}}
                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}" />--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label class="col-md-4 control-label">E-mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input type="email" class="form-control" name="email" value="{{ old('email') }}">--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input type="password" class="form-control" name="password">--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="remember"> Remember Me--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--<i class="fa fa-btn fa-sign-in"></i>Login--}}
                                {{--</button>--}}

                                {{--<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>--}}
                                {{--<a href="redirect">FB Login</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
@endsection
