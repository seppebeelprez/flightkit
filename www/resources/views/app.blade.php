@extends('layouts.frontoffice')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {{--@if (Auth::guest())--}}

            {{--@else--}}
                {{--{{ $title }}--}}
            {{--@endif--}}

            <md-content ng-app="app">
                <div ui-view="main"></div>
            </md-content>
        </div>
    </div>
</div>
@endsection
