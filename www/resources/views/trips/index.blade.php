@extends('layouts.frontoffice')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Trips</div>

                    <div class="panel-body">
                        A overview of your added trips!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
