/**
 * @author    Seppe Beelprez
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .config(Config);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Config.$inject = [
        // Angular
        '$compileProvider'
    ];

    function Config(
        // Angular
        $compileProvider
    ) {
        var debug = true; // Set to `false` for production
        $compileProvider.debugInfoEnabled(debug);
    }
})();



