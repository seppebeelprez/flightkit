<?php

Route::group(['prefix' => 'api/v1'], function () {
    Route::resource('flights', 'Flights\FlightsController');

    //Account
    Route::resource('account', 'Api\AccountController');
});
