<?php

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'MainController@index');

    Route::get('/redirect', 'SocialAuthController@redirect');
    Route::get('/callback', 'SocialAuthController@callback');

    Route::get('/home', 'MainController@index');
    Route::get('/account', 'Account\AccountController@index');

    Route::group(['prefix' => 'flights'], function () {
        Route::resource('/', 'Flights\FlightsController@index');
    });

    Route::group(['prefix' => 'trips'], function () {
        Route::resource('overview', 'Trips\TripsController@index');
    });
});