<?php

namespace App\Http\Controllers\Account;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * @Request
     */
    public function index()
    {
        $title = 'AccountController';

//        if (Auth::check())
//        {
//            $user = Auth::user();
//        }

        return view('app', compact('title'));
    }
}
