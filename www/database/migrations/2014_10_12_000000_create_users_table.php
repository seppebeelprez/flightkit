<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            // Data
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();;
            $table->string('password');
            $table->rememberToken();
//            $table->string('given_name');
//            $table->string('family_name');

            // Meta Data
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
