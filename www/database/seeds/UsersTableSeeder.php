<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'seppebeelprez@gmail.com',
            'name' => 'seppebeelprez',
            'password' => Hash::make('seppeseppe'),
//            'given_name' => 'Seppe',
//            'family_name' => 'Beelprez',
        ]);

        // Faker
        // -----
        factory(User::class, DatabaseSeeder::AMOUNT['NONE'])->create();
    }
}
