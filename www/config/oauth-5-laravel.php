<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '1692779330963372',
			'client_secret' => 'e15eb7202a8b68ffca5f7c2996a0007a',
			'scope'         => ['email','read_friendlists','user_online_presence'],
		],

	]

];